#ifndef DAMELLOPION_4_FRONT_GAMETYPES_H
#define DAMELLOPION_4_FRONT_GAMETYPES_H

enum GAME_TYPE
{
    GAME_TICTACTOE = 0,
    GAME_CONNECT4 = 1,
    GAME_REVERSI = 2,
    GAME_CHECKERS = 3
};

#endif //DAMELLOPION_4_FRONT_GAMETYPES_H
