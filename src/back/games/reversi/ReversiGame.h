#ifndef DAMELLOPION_4_FRONT_REVERSIGAME_H
#define DAMELLOPION_4_FRONT_REVERSIGAME_H

#include "../Game.h"
#include "../../grids/reversi/ReversiGrid.h"

class ReversiGame : public Game
{
public:
    ReversiGame();
    bool playTurn(std::pair<int, int> position);

    void getScores(int& scorePlayer1, int& scorePlayer2, int& scoreNeutral);

    GAME_TYPE getType() const override;
    Grid& getGrid() override;

private:
    ReversiGrid _grid;
};


#endif //DAMELLOPION_4_FRONT_REVERSIGAME_H
