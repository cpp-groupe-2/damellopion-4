#include "ReversiGame.h"
#include "../../../global/utils/Serializer.h"

ReversiGame::ReversiGame() : _grid(ReversiGrid()), Game(2)
{}

bool ReversiGame::playTurn(std::pair<int, int> position)
{
    if (_isFinished)
    {
        return false;
    }

    if (!_grid.isEmptyCell(position.first, position.second))
    {
        return false;
    }

    if (!_grid.checkOrFlip(getCurrentPlayer(), position.first, position.second, true))
    {
        return false;
    }

    if (_grid.isFull())
    {
        _isFinished = true;
    }

    changePlayer();

    //Loop until next playing player or end game if no one can play
    int playerSkipped = 0;
    for (int i = 0; i < _maxPlayers; i++)
    {
        if (_grid.canPlay(getCurrentPlayer()))
        {
            break;
        }
        playerSkipped++;
        changePlayer();
    }
    if (playerSkipped == _maxPlayers)
    {
        _isFinished = true;
        return true;
    }
    Serializer::serialize(*this, "latestReversi");

    return true;
}

void ReversiGame::getScores(int& scorePlayer1, int& scorePlayer2, int& scoreNeutral)
{
    _grid.getScores(scorePlayer1, scorePlayer2, scoreNeutral);
}

GAME_TYPE ReversiGame::getType() const
{
    return GAME_TYPE::GAME_REVERSI;
}

Grid& ReversiGame::getGrid()
{
    return _grid;
}
