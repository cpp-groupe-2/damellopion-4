#include "CheckersGame.h"
#include "../../../global/utils/Serializer.h"

CheckersGame::CheckersGame() : _grid(CheckersGrid()), Game(2) {}

bool CheckersGame::finishTurn()
{
    _grid.promotePawnIfValid(getCurrentPlayer());
    _grid.getScores(scorePlayer1,scorePlayer2);
    if(scorePlayer1 == 0 || scorePlayer2 == 0) {
        _isFinished = true;
        return true;
    }
    return false;
}

GAME_TYPE CheckersGame::getType() const {
    return GAME_CHECKERS;
}

Grid &CheckersGame::getGrid() {
    return _grid;
}

bool CheckersGame::playTurn(std::pair<int, int> pieceToMove, std::pair<int, int> destination)
{
    bool playerCanTake;
    bool pieceCanTake = false;
    playerCanTake = canPlayerTake(getCurrentPlayer());

    if(!_grid.isPlayerPiece(getCurrentPlayer(), pieceToMove.first, pieceToMove.second)){
        return false;
    }

    if(lastDestination.first != -1){
        if(lastDestination.first != pieceToMove.first || lastDestination.second != pieceToMove.second){
            return false;
        }
    }

    if(_grid.canTake(pieceToMove.first, pieceToMove.second, getCurrentPlayer())){
        pieceCanTake = true;
    }
    if(playerCanTake && !pieceCanTake){
        return false;
    }
        // Si une des pieces du joueur peux manger il est obligatoire de la selectionner par rapport à ceux qui ne peuvent que se déplacer
    if (tryToMovePiece(pieceToMove, destination)) {
        _grid.movePiece(pieceToMove, destination);
        Serializer::serialize(*this, "latestCheckers");
        changePlayer();
        return true;
    }
    else if (tryToTakePiece(pieceToMove, destination)) {
        _grid.takePiece(pieceToMove, destination, getCurrentPlayer());
        if(_grid.canTake(destination.first, destination.second, getCurrentPlayer())){
            lastDestination = {destination.first, destination.second};
        } else {
            changePlayer();
            Serializer::serialize(*this, "latestCheckers");
            lastDestination = {-1, -1};
        }
        return true;
    }
    else {
        return false;
    }
}


bool CheckersGame::tryToMovePiece(std::pair<int, int> pieceToMove, std::pair<int, int> destination) const {
    if (_grid.canTake(pieceToMove.first, pieceToMove.second, getCurrentPlayer())){
        return false;
    }
    if (_grid.pieceIsKing(pieceToMove.first, pieceToMove.second)) {
        return _grid.isValidKingMove(pieceToMove, destination);
    }
    else {
       return _grid.isValidPawnMove(pieceToMove, destination, getCurrentPlayer());
    }
}

bool CheckersGame::tryToTakePiece(std::pair<int, int> pieceToMove, std::pair<int, int> destination) const {
    if (_grid.pieceIsKing(pieceToMove.first,pieceToMove.second)) {
        return _grid.isValidKingTake(pieceToMove, destination, getCurrentPlayer());
    }
    else {
        return _grid.isValidPawnTake(pieceToMove, destination, getCurrentPlayer());
    }
}

bool CheckersGame::canPlayerTake(int player) const {
    for(int line = 0; line < _grid.getRowSize(); line++){
        for(int column = 0; column < _grid.getColumnSize(); column++){
            if(_grid.isPlayerPiece(player, line, column)){
                if(_grid.canTake(line, column, player)){
                    return true;
                }
            }
        }
    }
    return false;
}