#ifndef DAMELLOPION4_GAMECHECKERS_H
#define DAMELLOPION4_GAMECHECKERS_H

#include "../Game.h"
#include "../../grids/checkers/CheckersGrid.h"

class CheckersGame : public Game {
public:
    CheckersGame();
    bool finishTurn();
    GAME_TYPE getType() const override;
    Grid& getGrid() override;
    bool tryToMovePiece(std::pair<int, int> pieceToMove, std::pair<int, int> destination) const;
    bool tryToTakePiece(std::pair<int, int> pieceToMove, std::pair<int, int> destination) const;
    bool canPlayerTake(int player) const;
    bool playTurn(std::pair<int, int> pieceToMove, std::pair<int, int> destination);


private:
    CheckersGrid _grid;
    int scorePlayer1 = 0;
    int scorePlayer2 = 0;
    std::pair<int, int> lastDestination = {-1, -1};

};
#endif //DAMELLOPION4_GAMECHECKERS_H
