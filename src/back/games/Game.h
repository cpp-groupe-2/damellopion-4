#ifndef DAMELLOPION_4_FRONT_GAME_H
#define DAMELLOPION_4_FRONT_GAME_H

#include <vector>
#include "../enums/GameTypes.h"
#include "../grids/Grid.h"

class Game
{
public:
    explicit Game(int maxPlayers);
    virtual GAME_TYPE getType() const = 0;
    virtual Grid& getGrid() = 0;

    inline int getCurrentPlayer() const {return _currentPlayer; }
    inline bool isFinished() const { return _isFinished; }
    inline bool isDraw() const { return _isDraw; }

    void load(int currentPlayer, const std::vector<std::vector<Token>>& grid);

protected:
    int _currentPlayer = 0;

    int _maxPlayers;

    bool _isFinished = false;
    bool _isDraw = false;

    /**
    * Ask for restart the game
    */
    void restart();

    /**
    * Change the playing player
    */
    void changePlayer();
};


#endif //DAMELLOPION_4_FRONT_GAME_H
