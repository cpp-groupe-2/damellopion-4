#include "Game.h"

Game::Game(int maxPlayers) : _maxPlayers(maxPlayers) {}

void Game::restart()
{
    _isFinished = false;
    _currentPlayer = 0;
    getGrid().initArray();
}

void Game::changePlayer()
{
    _currentPlayer = (_currentPlayer + 1) % _maxPlayers;
}

void Game::load(int currentPlayer, const std::vector<std::vector<Token>>& grid)
{
    _currentPlayer = currentPlayer % _maxPlayers;
    getGrid().load(grid);
}
