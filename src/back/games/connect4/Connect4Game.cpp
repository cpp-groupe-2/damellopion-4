#include "Connect4Game.h"
#include "../../../global/token/TokenGenerator.h"
#include "../../../global/utils/Serializer.h"

Connect4Game::Connect4Game() : _grid(Connect4Grid()), Game(2) {}

bool Connect4Game::playTurn(int column)
{
    if (_isFinished)
    {
        return false;
    }

    int row = _grid.getFirstEmptyCell(column);

    if (row == -1)
    {
        return false;
    }

    Token token = TokenGenerator::getConnect4Token(getCurrentPlayer());
    _grid.addToken(token, row, column);

    // win or draw
    if (_grid.isWinner(getCurrentPlayer()))
    {
        _isFinished = true;
    }
    else if (_grid.isFull())
    {
        _isDraw = true;
        _isFinished = true;
    }
    else
    {
        changePlayer();
        Serializer::serialize(*this, "latestConnect4");
    }
    return true;
}

GAME_TYPE Connect4Game::getType() const {
    return GAME_TYPE::GAME_CONNECT4;
}

Grid& Connect4Game::getGrid()
{
    return _grid;
}
