#ifndef DAMELLOPION_4_FRONT_CONNECT4GAME_H
#define DAMELLOPION_4_FRONT_CONNECT4GAME_H

#include "../Game.h"
#include "../../grids/gridLine/connect4/Connect4Grid.h"

class Connect4Game : public Game
{
public:
    Connect4Game();
    bool playTurn(int column);

    GAME_TYPE getType() const override;
    Grid& getGrid() override;

private:
    Connect4Grid _grid;

};


#endif //DAMELLOPION_4_FRONT_CONNECT4GAME_H
