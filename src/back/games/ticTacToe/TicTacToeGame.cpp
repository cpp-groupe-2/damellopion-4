#include "TicTacToeGame.h"
#include "../../../global/token/TokenGenerator.h"
#include "../../../global/utils/Serializer.h"

TicTacToeGame::TicTacToeGame() : _grid(TicTacToeGrid()), Game(2) {}

bool TicTacToeGame::playTurn(std::pair<int, int> position)
{
    if (_isFinished)
    {
        return false;
    }

    if (!_grid.isEmptyCell(position.first, position.second))
    {
        return false;
    }

    Token token = TokenGenerator::getTicTacToeToken(getCurrentPlayer());
    _grid.addToken(token, position.first, position.second);

    // win or draw
    if (_grid.isWinner(getCurrentPlayer()))
    {
        _isFinished = true;
    }
    else if (_grid.isFull())
    {
        _isDraw = true;
        _isFinished = true;
    }
    else
    {
        changePlayer();
        Serializer::serialize(*this, "latestTicTacToe");
    }
    return true;
}

GAME_TYPE TicTacToeGame::getType() const
{
    return GAME_TYPE::GAME_TICTACTOE;
}

Grid &TicTacToeGame::getGrid()
{
    return _grid;
}
