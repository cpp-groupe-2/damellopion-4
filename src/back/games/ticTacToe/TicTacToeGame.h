#ifndef DAMELLOPION_4_FRONT_TICTACTOEGAME_H
#define DAMELLOPION_4_FRONT_TICTACTOEGAME_H

#include "../Game.h"
#include "../../grids/gridLine/ticTacToe/TicTacToeGrid.h"

class TicTacToeGame : public Game
{
public:
    TicTacToeGame();
    bool playTurn(std::pair<int, int> position);

    GAME_TYPE getType() const override;
    Grid& getGrid() override;

private:
    TicTacToeGrid _grid;
};


#endif //DAMELLOPION_4_FRONT_TICTACTOEGAME_H
