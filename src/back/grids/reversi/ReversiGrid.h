#ifndef DAMELLOPION_4_FRONT_REVERSIGRID_H
#define DAMELLOPION_4_FRONT_REVERSIGRID_H

#include "../Grid.h"

class ReversiGrid : public Grid
{
public:
    ReversiGrid();

    /**
    * Init the grid vector and place default token
    */
    void initArray();

    /**
    * Check if a token can be placed in a given position and if flip mode is true place/flip token
    * @param player The player who want to place the token
    * @param row The row
    * @param column The column
    * @param flipMode The flip mode (If true flip tokens else just check)
    * @return true if can place token / has flip tokens
    */
    bool checkOrFlip(int player, int row, int column, bool flipMode);

    /**
    * Recursively check if a token can be placed and if flip mode is true place/flip token in only one direction
    * @param player The player who want to place the token
    * @param row The row
    * @param column The column
    * @param xModifier The yModifier use for the direction
    * @param yModifier The xModifier use for the direction
    * @param first If this is the first entry in the function
    * @param flipMode The flip mode (If true flip tokens else just check)
    * @return true if can place token / has flip tokens
    */
    bool checkOrFlipDirection(int player, int row, int column, int xModifier, int yModifier, bool first, bool flipMode);

    /**
    * Check if the player can play
    * @param player The player who want to check the possibility to play
    * @return true if player can play
    */
    bool canPlay(int player);

    /**
    * Change int references passed has argument for score values
    * @param scorePlayer1 The reference that take the score of the player 1
    * @param scorePlayer2 The reference that take the score of the player 2
    * @param scoreNeutral The reference that take the score of neutral cells
    */
    void getScores(int& scorePlayer1, int& scorePlayer2, int& scoreNeutral);
};


#endif //DAMELLOPION_4_FRONT_REVERSIGRID_H
