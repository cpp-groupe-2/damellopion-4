#include "ReversiGrid.h"
#include "../../../global/token/TokenGenerator.h"

ReversiGrid::ReversiGrid() : Grid(8, 8)
{
    initArray();
}

void ReversiGrid::initArray()
{
    addToken(TokenGenerator::getReversiToken(1), 3, 3);
    addToken(TokenGenerator::getReversiToken(1), 4, 4);

    addToken(TokenGenerator::getReversiToken(0), 3, 4);
    addToken(TokenGenerator::getReversiToken(0), 4, 3);
}

bool ReversiGrid::checkOrFlip(int player, int row, int column, bool flipMode)
{
    bool hasFlip = false;
    for (int i = 0; i < 8; i++)
    {
        // Change direction modifier
        std::vector<std::vector<int>> directionModifier = {
                {-1, -1}, {-1, 0}, {-1, 1}, // 3 positions above
                {0, -1}, {0, 1}, // 2 positions on same row
                {1, -1}, {1, 0}, {1, 1} // 3 positions below
        };

        if (checkOrFlipDirection(player, row, column, directionModifier[i][0], directionModifier[i][1], true, flipMode))
        {
            hasFlip = true;
        }
    }
    if (hasFlip && flipMode)
    {
        addToken(TokenGenerator::getReversiToken(player), row, column);
    }

    return hasFlip;
}

bool ReversiGrid::checkOrFlipDirection(int player, int row, int column, int rowModifier, int columnModifier, bool first, bool flipMode)
{
    int nextRow = row + rowModifier;
    int nextColumn = column + columnModifier;

    if (nextRow < 0 || nextRow >= _columnSize
        || nextColumn < 0 || nextColumn >= _rowSize
        || isEmptyCell(nextRow, nextColumn))
    {
        return false;
    }

    if (_grid[nextRow][nextColumn].getPlayer() == player)
    {
        // If first return false (The same color side by side), otherwise return true
        return !first;
    }

    bool canFlip = checkOrFlipDirection(player, nextRow, nextColumn, rowModifier, columnModifier, false, flipMode);
    if (canFlip && flipMode)
    {
        addToken(TokenGenerator::getReversiToken(player), nextRow, nextColumn);
    }
    return canFlip;
}

bool ReversiGrid::canPlay(int player)
{
    for (int row = 0; row < _rowSize; row++)
    {
        for (int column = 0; column < _columnSize; column++)
        {
            if (checkOrFlip(player, row, column, false))
            {
                return true;
            }
        }
    }
    return false;
}

void ReversiGrid::getScores(int& scorePlayer1, int& scorePlayer2, int& scoreNeutral)
{
    int player1 = 0, player2 = 0, neutral = 0;
    for (int row = 0; row < _rowSize; row++)
    {
        for (int column = 0; column < _columnSize; column++)
        {
            if (_grid[row][column].getPlayer() == 0)
            {
                player1++;
            }
            else if (_grid[row][column].getPlayer() == 1)
            {
                player2++;
            }
            else
            {
                neutral++;
            }
        }
    }
    scorePlayer1 = player1;
    scorePlayer2 = player2;
    scoreNeutral = neutral;
}
