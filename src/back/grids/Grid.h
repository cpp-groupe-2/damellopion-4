#ifndef DAMELLOPION_4_FRONT_GRID_H
#define DAMELLOPION_4_FRONT_GRID_H

#include <vector>
#include "../../global/token/Token.h"

class Grid
{
public:
    Grid(int rowSize, int columnSize);
    virtual ~Grid() = default;

    inline int getColumnSize() const { return _columnSize; }
    inline int getRowSize() const { return _rowSize; }
    inline const std::vector<std::vector<Token>>& getGrid() { return _grid; };

    /**
    * Check if cell is empty
    * @param row The row
    * @param column The column
    * @return true if cell is empty
    */
    bool isEmptyCell(int row, int column) const;

    /**
    * Check if the grid is full
    * @return true if all cells have values
    */
    bool isFull() const;

    /**
    * Init the grid vector
    */
    void initArray();

    /**
    * Add token to the grid
    * @param Token The token to add
    * @param row The row
    * @param column The column
    */
    void addToken(const Token& token, int row, int column);

    void load(const std::vector<std::vector<Token>>& grid);

protected:
    std::vector<std::vector<Token>> _grid;
    const int _rowSize;
    const int _columnSize;
};


#endif //DAMELLOPION_4_FRONT_GRID_H
