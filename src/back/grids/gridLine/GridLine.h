#ifndef DAMELLOPION_4_FRONT_GRIDLINE_H
#define DAMELLOPION_4_FRONT_GRIDLINE_H

#include "../Grid.h"

class GridLine : public Grid
{
public:
    GridLine(int rowSize, int columnSize, int tokenInLine);

    ~GridLine() override = default;

    /**
    * Check if a player is the winner
    * @param player The player we want to check the win
    * @return true if player is the winner
    */
    bool isWinner(int player);

private:
    const int _tokenInLine;

    /**
    * Check if a row has been complete by a player according to the number of cells in row needed
    * @param row The number of the row to check
    * @param player The player we want to check if he has complete the line
    * @return true if line is completed
    */
    bool isRowComplete(int row, int player);

    /**
    * Check if a column has been complete by a player according to the number of cells in row needed
    * @param column The number of the column to check
    * @param player The player we want to check if he has complete the column
    * @return true if column is completed
    */
    bool isColumnComplete(int column, int player);

    /**
    * Check if at least one diagonal has been complete by a player according to the number of cells in row needed
    * @param player The player we want to check if he has complete diagonal
    * @return true if at least one diagonal is completed
    */
    bool isDiagonalComplete(int player);

};


#endif //DAMELLOPION_4_FRONT_GRIDLINE_H
