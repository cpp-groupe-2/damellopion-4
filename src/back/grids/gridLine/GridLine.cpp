#include "GridLine.h"

GridLine::GridLine(int rowSize, int columnSize, int tokenInLine) : _tokenInLine(tokenInLine), Grid(rowSize, columnSize) {}

bool GridLine::isWinner(int player)
{
    for (int i = 0; i < _columnSize; i++)
    {
        if (isColumnComplete(i, player))
        {
            return true;
        }
    }

    for (int i = 0; i < _rowSize; i++)
    {
        if (isRowComplete(i, player))
        {
            return true;
        }
    }

    return isDiagonalComplete(player);
}

bool GridLine::isRowComplete(int row, int player)
{
    int lineCounter = 0;
    for (int column = 0; column < _columnSize; column++)
    {
        if (_grid[row][column].getPlayer() == player)
        {
            lineCounter++;
        }
        else
        {
            lineCounter = 0;
        }

        if (lineCounter == _tokenInLine)
        {
            return true;
        }
    }
    return false;
}

bool GridLine::isColumnComplete(int column, int player)
{
    int lineCounter = 0;
    for (int y = _rowSize - 1; y >= 0; y--)
    {
        if (_grid[y][column].getPlayer() == player)
        {
            lineCounter++;
        }
        else
        {
            lineCounter = 0;
        }

        if (lineCounter == _tokenInLine)
        {
            return true;
        }
    }

    return false;
}

bool GridLine::isDiagonalComplete(int player)
{
    for (int row = 0; row <= _rowSize - _tokenInLine; row++)
    {
        for (int column = 0; column <= _columnSize - _tokenInLine; column++)
        {
            int lineCounter = 0;
            for (int i = 0; i < _tokenInLine; i++)
            {
                if (_grid[row + i][column + i].getPlayer() == player)
                {
                    lineCounter++;
                }
                if (lineCounter == _tokenInLine)
                {
                    return true;
                }
            }
        }
    }
    for (int row = 0; row <= _rowSize - _tokenInLine; row++)
    {
        for (int column = _columnSize - 1; column >= _columnSize - _tokenInLine; column--)
        {
            int cellNumber = 0;
            for (int i = 0; i < _tokenInLine; i++)
            {
                if (_grid[row + i][column - i].getPlayer() == player)
                {
                    cellNumber++;
                }
                if (cellNumber == _tokenInLine)
                {
                    return true;
                }
            }
        }
    }
    return false;
}
