#include "Connect4Grid.h"

Connect4Grid::Connect4Grid() : GridLine(6,7,4){}

int Connect4Grid::getFirstEmptyCell(int column) const
{
    for (int row = _rowSize - 1; row >= 0; row--)
    {
        if (isEmptyCell(row, column))
        {
            return row;
        }
    }
    return -1;
}
