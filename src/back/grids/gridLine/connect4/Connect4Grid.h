#ifndef DAMELLOPION_4_FRONT_CONNECT4GRID_H
#define DAMELLOPION_4_FRONT_CONNECT4GRID_H

#include "../GridLine.h"

class Connect4Grid : public GridLine
{
public:
    Connect4Grid();
    int getFirstEmptyCell(int column) const;
};


#endif //DAMELLOPION_4_FRONT_CONNECT4GRID_H
