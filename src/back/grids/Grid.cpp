#include "Grid.h"

Grid::Grid(int rowSize, int columnSize) : _rowSize(rowSize), _columnSize(columnSize)
{
    initArray();
}

bool Grid::isFull() const
{
    for (int row = 0; row < _rowSize; row++)
    {
        for (int column = 0; column < _columnSize; column++)
        {
            if (isEmptyCell(row, column))
            {
                return false;
            }
        }
    }
    return true;
}

bool Grid::isEmptyCell(int row, int column) const
{
   return _grid[row][column].getPlayer() == -1;
}

void Grid::initArray()
{
    _grid = std::vector<std::vector<Token>>(_rowSize, std::vector<Token>(_columnSize));
}

void Grid::addToken(const Token& token,  int row, int column)
{
    _grid[row][column] = token;
}

void Grid::load(const std::vector<std::vector<Token>> &grid)
{
    for (int row = 0; row < _rowSize && row < grid.size(); row++)
    {
        const std::vector<Token> line = grid[row];
        for (int column = 0; column < _columnSize && column < line.size(); column++)
        {
            _grid[row][column] = line[column];
        }
    }
}
