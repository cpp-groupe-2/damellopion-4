#include "CheckersGrid.h"
#include "../../../global/token/TokenGenerator.h"
#include <cmath>

CheckersGrid::CheckersGrid() : Grid(10, 10)
{
    initBoard();
}

void CheckersGrid::initBoard() {
    for (int column = 0; column < getColumnSize(); column++){
        // Initialize the first two lines at the top
        for (int line = 0; line < 4; line++){
            if (column % 2 == 1 && line % 2 == 0 || column % 2 == 0 && line % 2 == 1){
                addToken(TokenGenerator::getCheckersToken(1, 0),line ,column);
            }
        }
        // Initialize the last two lines at the bottom
        for (int line = getRowSize() - 4; line < getRowSize(); line++){
            if (column % 2 == 1 && line % 2 == 0 || column % 2 == 0 && line % 2 == 1){
                addToken(TokenGenerator::getCheckersToken(0, 0),line ,column);
            }
        }
    }
}

bool CheckersGrid::coordinatesAreInGrid(int line, int column) const {
    return line >= 0 && line < getRowSize() && column >= 0 && column < getRowSize();
}

DIAGONAL_DIRECTION CheckersGrid::getDirection(std::pair<int, int> start, std::pair<int, int> end) const{
    if (start.first > end.first && start.second < end.second){
        return DIAGONAL_DIRECTION::UP_RIGHT;
    }
    else if (start.first > end.first && start.second > end.second){
        return DIAGONAL_DIRECTION::UP_LEFT;
    }
    else if (start.first < end.first && start.second < end.second){
        return DIAGONAL_DIRECTION::DOWN_RIGHT;
    }
    else if (start.first < end.first && start.second > end.second){
        return DIAGONAL_DIRECTION::DOWN_LEFT;
    }
}

std::pair<int,int> CheckersGrid::getNextCoordinatesDiagonal(std::pair<int, int> start, DIAGONAL_DIRECTION direction, int increment) const {
    switch(direction){
        case DIAGONAL_DIRECTION::UP_RIGHT:
            return std::make_pair(start.first - increment, start.second + increment);
        case DIAGONAL_DIRECTION::UP_LEFT:
            return std::make_pair(start.first - increment, start.second - increment);
        case DIAGONAL_DIRECTION::DOWN_RIGHT:
            return std::make_pair(start.first + increment, start.second + increment);
        case DIAGONAL_DIRECTION::DOWN_LEFT:
            return std::make_pair(start.first + increment, start.second - increment);
    }
}

bool CheckersGrid::isValidPawnMove(std::pair<int, int> start, std::pair<int, int> end, int player) const{
    if (!isEmptyCell(end.first, end.second)){
        return false;
    }
    if (abs(start.first - end.first) != 1 || abs(start.second - end.second) != 1) {
        return false;
    }
    DIAGONAL_DIRECTION direction = getDirection(start, end);
    if (isPlayingWhite(player)){
        return direction == DIAGONAL_DIRECTION::UP_RIGHT || direction == DIAGONAL_DIRECTION::UP_LEFT;
    }
    return direction == DIAGONAL_DIRECTION::DOWN_RIGHT || direction == DIAGONAL_DIRECTION::DOWN_LEFT;
}

bool CheckersGrid::isValidPawnTake(std::pair<int, int> start, std::pair<int, int> end, int player) const {
    if (!isEmptyCell(end.first, end.second)){
        return false;
    }
    if (abs(start.first - end.first) == 2 && abs(start.second - end.second) == 2) {
        int betweenLine = (start.first + end.first) / 2;
        int betweenColumn = (start.second + end.second) / 2;
        return isOpponentPiece(player, betweenLine, betweenColumn);
    }
    return false;
}

bool CheckersGrid::isValidKingMove(std::pair<int, int> start, std::pair<int, int> end) const {
    if (!isEmptyCell(end.first, end.second)){
        return false;
    }

    DIAGONAL_DIRECTION direction = getDirection(start, end);
    for (int i = 1; i < abs(start.first - end.first); i++) {
        std::pair <int,int> coords = getNextCoordinatesDiagonal(start, direction, i);

        if (!coordinatesAreInGrid(coords.first, coords.second)) {
            return false;
        }
        if (!isEmptyCell(coords.first, coords.second)) {
            return false;
        }
    }
    return true;
}

bool CheckersGrid::isValidKingTake(std::pair<int, int> start, std::pair<int, int> end, int player) const {
    if (!isEmptyCell(end.first, end.second)){
        return false;
    }

    DIAGONAL_DIRECTION direction = getDirection(start, end);
    int opponentPieces = 0;
    int i = 1;
    std::pair<int, int> coords = getNextCoordinatesDiagonal(start, direction, i);

        while(coords.first != end.first && coords.second != end.second){
            if (!coordinatesAreInGrid(coords.first, coords.second)) {
                break;
            }
            if (isPlayerPiece(player, coords.first, coords.second)) {
                return false;
            }
            if (isOpponentPiece(player, coords.first, coords.second)) {
                opponentPieces++;
            }
            i++;
            coords = getNextCoordinatesDiagonal(start, direction, i);
        }

    return opponentPieces == 1 && isEmptyCell(end.first,end.second);
}

std::pair<int,int> CheckersGrid::getOpponentPieceInTake(std::pair<int, int> start, std::pair<int, int> end, int player) const {
    DIAGONAL_DIRECTION direction = getDirection(start, end);
    std::pair<int, int> coords;

    for (int i = 1; i < abs(start.first - end.first); i++) {
        coords = getNextCoordinatesDiagonal(start, direction, i);

        if (!coordinatesAreInGrid(coords.first, coords.second)) {
            break;
        }
        if (isOpponentPiece(player, coords.first, coords.second)) {
            return coords;
        }
    }
    return coords;
}

bool CheckersGrid::isPlayingWhite(int player) const {
    return player == 0;
}

bool CheckersGrid::isPlayerPiece(int player, int line, int column) const {
    return player == _grid[line][column].getPlayer();
}

bool CheckersGrid::isOpponentPiece(int player, int line, int column) const {
    return !isEmptyCell(line,column) && !isPlayerPiece(player, line, column);
}

void CheckersGrid::movePiece(std::pair<int, int> start, std::pair<int, int> end) {
    Token piece = _grid[start.first][start.second];
    _grid[end.first][end.second] = piece;
    addToken(Token(-1, -1, ""), start.first,start.second);
}

void CheckersGrid::takePiece(std::pair<int, int> start, std::pair<int, int> end, int player) {
    std::pair<int,int> opponentPiece = getOpponentPieceInTake(start, end, player);
    addToken(Token(-1, -1, ""), opponentPiece.first,opponentPiece.second);
    movePiece(start, end);
}

bool CheckersGrid::pawnCanTake(int line, int column, int player) const {
    std::vector<std::vector<int>> directionModifier = {
            {-1, -1},{-1, 1},
            {1, -1}, {1,  1}
    };

    for (int i = 0; i < 4; i++) {
        int lineChanged = line + directionModifier[i][1];
        int columnChanged = column + directionModifier[i][0];
        if (!coordinatesAreInGrid(lineChanged, columnChanged)) {
            continue;
        }
        if (isOpponentPiece(player, lineChanged, columnChanged)) {
            lineChanged += directionModifier[i][1];
            columnChanged += directionModifier[i][0];
            if(coordinatesAreInGrid(lineChanged, columnChanged)){
                if(isEmptyCell(lineChanged,columnChanged)) {
                    return true;
                }
            }
        }
    }
    return false;
}

bool CheckersGrid::kingCanTake(int line, int column, int player) const {
    std::vector<DIAGONAL_DIRECTION> directions = {
            DIAGONAL_DIRECTION::UP_LEFT,
            DIAGONAL_DIRECTION::UP_RIGHT,
            DIAGONAL_DIRECTION::DOWN_LEFT,
            DIAGONAL_DIRECTION::DOWN_RIGHT
    };

    for (auto direction : directions){
        for (int i = 1; i < getRowSize(); i++) {
            std::pair start = std::make_pair(line, column);
            std::pair<int,int> end = getNextCoordinatesDiagonal(start, direction, i);
            if(!coordinatesAreInGrid(end.first, end.second)) {
                break;
            }
            if (isValidKingTake(start, end, player)) {
                return true;
            }
        }
    }
    return false;
}

bool CheckersGrid::canTake(int line, int column, int player) const {
    if (pieceIsKing(line, column)) {
        return kingCanTake(line, column, player);
    }
    else {
        return pawnCanTake(line, column, player);
    }
}

bool CheckersGrid::pieceIsKing(int line, int column) const {
    return _grid[line][column].getTokenType() == 1;
}

void CheckersGrid::promotePawnIfValid(int player) {
   for (int i = 0; i < getColumnSize(); i++){
       if (!isPlayingWhite(player)){
           if(_grid[0][i].getTokenType() == 0 && _grid[0][i].getPlayer() == 0){
               addToken(TokenGenerator::getCheckersToken(0, 1), 0 , i);
           }
       }
       else {
           if(_grid[getRowSize() - 1][i].getTokenType() == 0 && _grid[getRowSize() - 1][i].getPlayer() == 1){
               addToken(TokenGenerator::getCheckersToken(1, 1), getRowSize() - 1 , i);
           }
       }
   }
}

void CheckersGrid::getScores(int &scoreWhitePlayer, int &scoreBlackPlayer) const {
    int player1 = 0, player2 = 0;
    for (int columns = 0; columns < getColumnSize(); columns++)
    {
        for (int lines = 0; lines < getRowSize(); lines++)
        {
            if (_grid[lines][columns].getPlayer() == 0)
            {
                player1++;
            }
            else if (_grid[lines][columns].getPlayer() == 1)
            {
                player2++;
            }
        }
    }
    scoreWhitePlayer = player1;
    scoreBlackPlayer = player2;
}