#ifndef DAMELLOPION4_CHECKERSGRID_H
#define DAMELLOPION4_CHECKERSGRID_H


#include "../Grid.h"
#include "../../enums/DiagonalsDirections.h"

class CheckersGrid : public Grid{

public:
    CheckersGrid();
    void initBoard();
    bool isPlayerPiece(int player, int line, int column) const;
    bool isOpponentPiece(int player, int line, int column) const;
    void movePiece(std::pair<int, int> start, std::pair<int, int> end);
    void takePiece(std::pair<int, int> start, std::pair<int, int> end, int player);
    bool canTake(int line, int column, int player) const;
    bool isPlayingWhite(int player) const;
    void promotePawnIfValid(int player);
    bool pieceIsKing(int line, int column) const;
    void getScores(int &scoreWhitePlayer, int &scoreBlackPlayer) const;
    DIAGONAL_DIRECTION getDirection(std::pair<int, int> start, std::pair<int, int> end) const;
    bool isValidPawnMove(std::pair<int, int> start, std::pair<int, int> end, int player) const;
    bool isValidPawnTake(std::pair<int, int> start, std::pair<int, int> end, int player) const;
    bool isValidKingMove(std::pair<int, int> start, std::pair<int, int> end) const;
    bool isValidKingTake(std::pair<int, int> start, std::pair<int, int> end, int player) const;

private:
    bool coordinatesAreInGrid(int line, int column) const;
    std::pair<int, int> getNextCoordinatesDiagonal(std::pair<int, int> start, DIAGONAL_DIRECTION direction, int increment) const;
    std::pair<int, int> getOpponentPieceInTake(std::pair<int, int> start, std::pair<int, int> end, int player) const;
    bool pawnCanTake(int line, int column, int player) const;
    bool kingCanTake(int line, int column, int player) const;
};


#endif //DAMELLOPION4_CHECKERSGRID_H
