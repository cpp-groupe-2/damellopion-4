#include <iostream>

#include "StringHelper.h"

std::vector<std::string> StringHelper::split(const std::string& toSplit, char delimiter)
{
    std::vector<std::string> splitted;

    unsigned long prev = 0, pos = 0;
    while (pos < toSplit.length() && prev < toSplit.length())
    {
        pos = toSplit.find(delimiter, prev);
        if (pos == std::string::npos)
        {
            pos = toSplit.length();
        }
        std::string substring = toSplit.substr(prev, pos-prev);
        splitted.push_back(substring);
        prev = pos + 1;
    }

    return splitted;
}
