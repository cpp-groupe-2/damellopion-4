#ifndef DAMELLOPION_4_FRONT_SERIALIZER_H
#define DAMELLOPION_4_FRONT_SERIALIZER_H

#include <iostream>
#include <filesystem>
#include <memory>
#include <QString>

#include "../../back/games/Game.h"
#include "../gameInfo/GameInfo.h"

class Serializer
{
public:
    static void serialize(Game& game, const std::string& fileName);
    static GameInfo deserialize(const QString& filePath);
    static std::vector<std::string> retrieveSaves();

private:
    Serializer() = default;
};


#endif //DAMELLOPION_4_FRONT_SERIALIZER_H
