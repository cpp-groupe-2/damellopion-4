#ifndef DAMELLOPION_4_FRONT_STRINGHELPER_H
#define DAMELLOPION_4_FRONT_STRINGHELPER_H

#include <vector>

class StringHelper
{
public:
    static std::vector<std::string> split(const std::string& toSplit, char delimiter);

private:
    StringHelper() = default;
};


#endif //DAMELLOPION_4_FRONT_STRINGHELPER_H
