#include <vector>
#include <fstream>

#include "Serializer.h"
#include "StringHelper.h"
#include "../token/TokenGenerator.h"

void Serializer::serialize(Game &game, const std::string &fileName)
{
    std::filesystem::create_directories("./saves");
    Grid& gameGrid = game.getGrid();
    std::ofstream file;
    file.open("saves/" + fileName + ".save");
    GAME_TYPE type = game.getType();
    file << type << "," << game.getCurrentPlayer() << std::endl;

    const std::vector<std::vector<Token>>& grid = gameGrid.getGrid();

    for (int row = 0; row< gameGrid.getRowSize(); row++)
    {
        for (int column = 0; column < gameGrid.getColumnSize(); column++)
        {
            const Token& token = grid[row][column];
            file << token.getTokenType() << "|" << token.getPlayer() ;
            if (column < gameGrid.getColumnSize() - 1)
            {
                file << ",";
            }
            else
            {
                file << std::endl;
            }
        }
    }
    file.close();
}

std::vector<std::string> Serializer::retrieveSaves()
{
    std::filesystem::create_directories("./saves");

    std::vector<std::string> saves = std::vector<std::string>();
    for (const auto& entry : std::filesystem::recursive_directory_iterator("./saves"))
    {
        if (entry.path().extension() == ".save")
        {
            saves.push_back(entry.path().stem().string());
        }
    }
    return saves;
}

GameInfo Serializer::deserialize(const QString& filePath)
{
    std::filesystem::create_directories("./saves");

    std::ifstream file;
    file.open("saves/" + filePath.toStdString() + ".save");

    std::string line;
    std::getline(file, line);

    std::vector<std::string> splittedLine = StringHelper::split(line, ',');

    if (splittedLine.size() < 2)
    {
        throw std::invalid_argument("Malformed save file");
    }

    int type = std::stoi(splittedLine[0]);
    int currentPlayer = std::stoi(splittedLine[1]);

    std::vector<std::vector<Token>> grid;

    while (std::getline(file, line))
    {
        splittedLine = StringHelper::split(line, ',');
        std::vector<Token> cells;
        for (const std::string& str : splittedLine)
        {
            const std::vector<std::string> tokenLine = StringHelper::split(str, '|');
            Token token = Token(-1, -1, "");

            if (tokenLine.size() >= 2)
            {
                const int tokenType = std::stoi(tokenLine[0]);
                const int player = std::stoi(tokenLine[1]);
                token = TokenGenerator::getTokenByGameType(type, player, tokenType);
            }

            // Emplace avoid creating temp object
            cells.emplace_back(token);
        }
        grid.push_back(cells);
    }

    file.close();

    return {type, currentPlayer, grid};
}
