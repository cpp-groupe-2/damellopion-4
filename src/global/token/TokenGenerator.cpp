#include "TokenGenerator.h"
#include "../../back/enums/GameTypes.h"

Token TokenGenerator::getTicTacToeToken(int player)
{
    std::string imagePath = getBasePath() + "ticTacToe/";

    if (player == 0)
    {
        imagePath += "X.svg";
    }
    else if (player == 1)
    {
        imagePath += "O.svg";
    }

    return {player, 0, imagePath};
}

Token TokenGenerator::getConnect4Token(int player)
{
    std::string imagePath = getBasePath() + "connect4/";

    if (player == 0)
    {
        imagePath += "red.svg";
    }
    else if (player == 1)
    {
        imagePath += "yellow.svg";
    }

    return {player, 0, imagePath};
}


Token TokenGenerator::getReversiToken(int player)
{
    std::string imagePath = getBasePath() + "reversi/";

    if (player == 0)
    {
        imagePath += "black.svg";
    }
    else if (player == 1)
    {
        imagePath += "white.svg";
    }

    return {player, 0, imagePath};
}

Token TokenGenerator::getCheckersToken(int player, int tokenType)
{
    std::string imagePath = getBasePath() + "checkers/";

    if (player == 0 )
    {
        if (tokenType == 0)
        {
            imagePath += "white.svg";
        }
        else if (tokenType == 1)
        {
            imagePath += "whiteKing.svg";
        }
    }
    else if (player == 1)
    {
        if (tokenType == 0)
        {
            imagePath += "black.svg";
        }
        else if (tokenType == 1)
        {
            imagePath += "blackKing.svg";
        }
    }

    return {player, tokenType, imagePath};
}

std::string TokenGenerator::getBasePath()
{
    return "../public/img/";
}

Token TokenGenerator::getTokenByGameType(int gameType, int player, int type)
{
    switch (gameType)
    {
        case GAME_TYPE::GAME_TICTACTOE:
            return getTicTacToeToken(player);
        case GAME_TYPE::GAME_CONNECT4:
            return getConnect4Token(player);
        case GAME_TYPE::GAME_REVERSI:
            return getReversiToken(player);
        case GAME_TYPE::GAME_CHECKERS:
            return getCheckersToken(player, type);
        default:
            return {-1, -1, ""};
    }
}