#ifndef DAMELLOPION_4_FRONT_TOKENGENERATOR_H
#define DAMELLOPION_4_FRONT_TOKENGENERATOR_H

#include "Token.h"
#include "../../back/enums/GameTypes.h"

class TokenGenerator
{
public:
    static Token getTicTacToeToken(int player);
    static Token getConnect4Token(int player);
    static Token getReversiToken(int player);
    static Token getCheckersToken(int player, int tokenType);

    static Token getTokenByGameType(int gameType, int player, int type);
private:
    TokenGenerator() = default;
    static std::string getBasePath();
};


#endif //DAMELLOPION_4_FRONT_TOKENGENERATOR_H
