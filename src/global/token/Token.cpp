#include "Token.h"

#include <utility>

Token::Token() : Token(-1, -1, "") {}

Token::Token(int player, int tokenType, std::string imagePath): _player(player), _tokenType(tokenType), _imagePath(std::move(imagePath)) {}
