//
// Created by Fyzoriel on 18/01/2022.
//

#ifndef DAMELLOPION_4_FRONT_TOKEN_H
#define DAMELLOPION_4_FRONT_TOKEN_H

#include <string>

class Token
{
public:
    Token();
    Token(int player, int tokenType, std::string imagePath);
    inline int getPlayer() const { return _player; }
    inline int getTokenType() const {return _tokenType; }
    inline const std::string& getImagePath() const { return _imagePath; }

private:
    int _player;
    int _tokenType;
    std::string _imagePath;
};


#endif //DAMELLOPION_4_FRONT_TOKEN_H
