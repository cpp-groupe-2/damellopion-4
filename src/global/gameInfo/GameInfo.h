#ifndef DAMELLOPION_4_FRONT_GAMEINFO_H
#define DAMELLOPION_4_FRONT_GAMEINFO_H

#include <vector>
#include "../token/Token.h"

class GameInfo
{
public:
    GameInfo(int gameType, int currentPlayer, std::vector<std::vector<Token>> grid);

    inline int getGameType() const { return _gameType; }
    inline int getCurrentPlayer() const { return _currentPlayer; }
    inline std::vector<std::vector<Token>> getGrid() const { return _grid; }

private:
    int _gameType;
    int _currentPlayer;
    std::vector<std::vector<Token>> _grid;
};


#endif //DAMELLOPION_4_FRONT_GAMEINFO_H
