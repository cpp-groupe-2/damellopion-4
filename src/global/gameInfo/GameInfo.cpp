#include "GameInfo.h"

#include <utility>

GameInfo::GameInfo(int gameType, int currentPlayer, std::vector<std::vector<Token>>  grid)
    : _gameType(gameType), _currentPlayer(currentPlayer), _grid(std::move(grid)) {}
