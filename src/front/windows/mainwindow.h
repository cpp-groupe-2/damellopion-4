#ifndef DAMELLOPION_4_FRONT_MAINWINDOW_H
#define DAMELLOPION_4_FRONT_MAINWINDOW_H

#include <QMainWindow>
#include "../components/screens/menus/MainMenu.h"
#include "../components/screens/menus/gameMenu/GameMenu.h"
#include "../components/screens/layouts/GameLayout.h"
#include "../components/screens/layouts/ticTacToe/TicTacToeLayout.h"
#include "../components/screens/layouts/connect4/Connect4Layout.h"
#include "../components/screens/layouts/reversi/ReversiLayout.h"
#include "../components/screens/layouts/checkers/CheckersLayout.h"
#include "../components/screens/menus/saveMenu/SaveMenu.h"
#include "../../back/games/ticTacToe/TicTacToeGame.h"
#include "../../back/games/connect4/Connect4Game.h"
#include "../../back/games/reversi/ReversiGame.h"
#include "../../global/gameInfo/GameInfo.h"

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

signals:

public slots:
    void showMainMenu();

    void showGameMenu();
    void showSaveMenu();

    void showTicTacToeLayout();
    void showConnect4Layout();
    void showReversiLayout();
    void showCheckersLayout();

    void showTicTacToeLayout(const GameInfo& info);
    void showConnect4Layout(const GameInfo& info);
    void showReversiLayout(const GameInfo& info);
    void showCheckersLayout(const GameInfo& info);

    void loadSave(const QString& path);

private:
    QWidget* _activeWidget;

    MainMenu* _mainMenu;
    GameMenu* _gameMenu;

    SaveMenu* _saveMenu;
};


#endif //DAMELLOPION_4_FRONT_MAINWINDOW_H
