#include <QPushButton>
#include <iostream>
#include "mainwindow.h"
#include "../../global/utils/Serializer.h"
#include "QtGui/QIcon"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setWindowTitle("Damellopion-4");
    setWindowIcon(QIcon("../public/img/icon/logo_damellopion4.png"));

    setFixedSize(900,900);
    auto *centralWidget = new QWidget(this);
    auto *verticalLayout = new QVBoxLayout(centralWidget);
    centralWidget->setLayout(verticalLayout);
    setCentralWidget(centralWidget);

    _mainMenu = new MainMenu(this);
    _gameMenu = new GameMenu(this);

    _gameMenu->hide();

    _saveMenu = new SaveMenu(this);

    _gameMenu->hide();
    _saveMenu->hide();


    _activeWidget = _mainMenu;

    verticalLayout->addWidget(_mainMenu);
    verticalLayout->addWidget(_gameMenu);
    verticalLayout->addWidget(_saveMenu);
}

void MainWindow::showMainMenu()
{
    _activeWidget->hide();
    _activeWidget = _mainMenu;
    _activeWidget->show();
}

void MainWindow::showSaveMenu()
{
    _activeWidget->hide();
    _activeWidget = new SaveMenu(this);
    _activeWidget->show();
}

void MainWindow::showGameMenu()
{
    _activeWidget->hide();
    _activeWidget = _gameMenu;
    _activeWidget->show();
}

void MainWindow::showTicTacToeLayout()
{
    _activeWidget->hide();
    _activeWidget = new TicTacToeLayout(this);
    _activeWidget->show();
}

void MainWindow::showConnect4Layout()
{
    _activeWidget->hide();
    _activeWidget = new Connect4Layout(this);
    _activeWidget->show();
}

void MainWindow::showReversiLayout()
{
    _activeWidget->hide();
    _activeWidget = new ReversiLayout(this);
    _activeWidget->show();
}

void MainWindow::showCheckersLayout()
{
    _activeWidget->hide();
    _activeWidget = new CheckersLayout(this);
    _activeWidget->show();
}

void MainWindow::showTicTacToeLayout(const GameInfo& info)
{
    _activeWidget->hide();
    _activeWidget = new TicTacToeLayout(this, info);
    _activeWidget->show();
}

void MainWindow::showConnect4Layout(const GameInfo& info)
{
    _activeWidget->hide();
    _activeWidget = new Connect4Layout(this, info);
    _activeWidget->show();
}

void MainWindow::showReversiLayout(const GameInfo& info)
{
    _activeWidget->hide();
    _activeWidget = new ReversiLayout(this, info);
    _activeWidget->show();
}

void MainWindow::showCheckersLayout(const GameInfo &info)
{
    _activeWidget->hide();
    _activeWidget = new CheckersLayout(this, info);
    _activeWidget->show();
}

void MainWindow::loadSave(const QString& path)
{
    GameInfo info = Serializer::deserialize(path);
    int type = info.getGameType();

    switch (type)
    {
        case GAME_TYPE::GAME_TICTACTOE:
            showTicTacToeLayout(info);
            break;

        case GAME_TYPE::GAME_CONNECT4:
            showConnect4Layout(info);
            break;

        case GAME_TYPE::GAME_REVERSI:
            showReversiLayout(info);
            break;
        case GAME_TYPE::GAME_CHECKERS:
            showCheckersLayout(info);
            break;
        default:
            showMainMenu();
    }
}
