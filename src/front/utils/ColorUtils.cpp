//
// Created by Fyzoriel on 12/01/2022.
//

#include <QBrush>
#include <QAbstractGraphicsShapeItem>
#include "ColorUtils.h"

void ColorUtils::setBgColor(QAbstractGraphicsShapeItem* shapeItem, QColor color)
{
    QBrush brush;
    brush.setStyle((Qt::SolidPattern));
    brush.setColor(color);
    shapeItem->setBrush(brush);
}
