#ifndef DAMELLOPION_4_FRONT_COLORUTILS_H
#define DAMELLOPION_4_FRONT_COLORUTILS_H


class ColorUtils {
public:
    ColorUtils() = delete;
    static void setBgColor(QAbstractGraphicsShapeItem* shapeItem, QColor color);
};


#endif //DAMELLOPION_4_FRONT_COLORUTILS_H
