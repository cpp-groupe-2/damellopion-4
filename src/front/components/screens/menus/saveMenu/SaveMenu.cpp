#include <QVBoxLayout>
#include <QPushButton>
#include <QSignalMapper>
#include "SaveMenu.h"
#include "../../../../../global/utils/Serializer.h"

SaveMenu::SaveMenu(QWidget *parentWidget)
{
    this->setParent(parentWidget);

    auto* mainLayout = new QVBoxLayout(this);

    mainLayout->setSpacing(20);
    mainLayout->setAlignment(Qt::AlignHCenter | Qt::AlignTop);

    std::vector<std::string> saves = Serializer::retrieveSaves();

    auto* mapper = new QSignalMapper(this);

    for (const auto& save : saves)
    {
        auto* button = new QPushButton(QString::fromStdString("load " + save), this);

        connect(button, SIGNAL(clicked()), mapper, SLOT(map()));
        mapper->setMapping(button,QString::fromStdString(save));

        mainLayout->addWidget(button);
    }
    connect(mapper, SIGNAL(mappedString(QString)), this->parent(), SLOT(loadSave(QString)));

    auto* returnButton = new QPushButton("return to main menu", this);
    QObject::connect(returnButton, SIGNAL(clicked()), this->parent(), SLOT(showMainMenu()));
    mainLayout->addWidget(returnButton);
}
