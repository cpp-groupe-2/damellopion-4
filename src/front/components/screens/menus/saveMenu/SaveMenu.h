#ifndef DAMELLOPION_4_FRONT_SAVEMENU_H
#define DAMELLOPION_4_FRONT_SAVEMENU_H

#include <QWidget>

class SaveMenu : public QWidget
{
public:
    explicit SaveMenu(QWidget* parentWidget);
};


#endif //DAMELLOPION_4_FRONT_SAVEMENU_H
