#ifndef DAMELLOPION_4_FRONT_MAINMENU_H
#define DAMELLOPION_4_FRONT_MAINMENU_H

#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QPushButton>

class MainMenu : public QWidget
{
public:
    MainMenu(QWidget* parentWidget);

signals:

public slots:

private:
    QPushButton* _startButton;
    QPushButton* _loadButton;
    QPushButton* _quitButton;
};


#endif //DAMELLOPION_4_FRONT_MAINMENU_H
