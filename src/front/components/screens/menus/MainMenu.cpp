#include <QVBoxLayout>
#include "MainMenu.h"

MainMenu::MainMenu(QWidget *parentWidget)
{
    auto* layout = new QVBoxLayout(this);
    layout->setSpacing(20);
    layout->setAlignment(Qt::AlignHCenter | Qt::AlignTop);

    this->setParent(parentWidget);
    _startButton = new QPushButton("Commencez un jeu", this);
    QObject::connect(_startButton, SIGNAL(clicked()), this->parent(), SLOT(showGameMenu()));

    _loadButton = new QPushButton("Charger un jeu", this);
    QObject::connect(_loadButton, SIGNAL(clicked()), this->parent(), SLOT(showSaveMenu()));

    _quitButton = new QPushButton("Quitter", this);
    QObject::connect(_quitButton, SIGNAL(clicked()), qApp, SLOT(quit()));

    layout->addWidget(_startButton);
    layout->addWidget(_loadButton);
    layout->addWidget(_quitButton);
}