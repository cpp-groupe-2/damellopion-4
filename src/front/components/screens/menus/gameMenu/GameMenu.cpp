#include <QVBoxLayout>
#include "GameMenu.h"

GameMenu::GameMenu(QWidget *parentWidget)
{
    auto* layout = new QVBoxLayout(this);
    layout->setSpacing(20);
    layout->setAlignment(Qt::AlignHCenter | Qt::AlignTop);

    this->setParent(parentWidget);
    _ticTacToeButton = new QPushButton("Tic Tac Toe", this);
    QObject::connect(_ticTacToeButton, SIGNAL(clicked()), this->parent(), SLOT(showTicTacToeLayout()));

    _connect4Button = new QPushButton("Connect 4", this);
    QObject::connect(_connect4Button, SIGNAL(clicked()), this->parent(), SLOT(showConnect4Layout()));

    _reversiButton = new QPushButton("Reversi", this);
    QObject::connect(_reversiButton, SIGNAL(clicked()), this->parent(), SLOT(showReversiLayout()));

    _checkersButton = new QPushButton("Checkers", this);
    QObject::connect(_checkersButton, SIGNAL(clicked()), this->parent(), SLOT(showCheckersLayout()));

    _returnButton = new QPushButton("return to main menu", this);
    QObject::connect(_returnButton, SIGNAL(clicked()), this->parent(), SLOT(showMainMenu()));

    layout->addWidget(_ticTacToeButton);
    layout->addWidget(_connect4Button);
    layout->addWidget(_reversiButton);
    layout->addWidget(_checkersButton);
    layout->addWidget(_returnButton);
}