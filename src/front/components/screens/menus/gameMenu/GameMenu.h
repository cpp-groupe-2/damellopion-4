#ifndef DAMELLOPION_4_FRONT_GAMEMENU_H
#define DAMELLOPION_4_FRONT_GAMEMENU_H

#include <QApplication>
#include <QWidget>
#include <QPushButton>

class GameMenu : public QWidget
{
public:
    explicit GameMenu(QWidget* parentWidget);

private:
    QPushButton* _ticTacToeButton;

    QPushButton* _connect4Button;

    QPushButton* _reversiButton;

    QPushButton* _checkersButton;

    QPushButton* _returnButton;
};


#endif //DAMELLOPION_4_FRONT_GAMEMENU_H
