#ifndef DAMELLOPION_4_FRONT_CHECKERSLAYOUT_H
#define DAMELLOPION_4_FRONT_CHECKERSLAYOUT_H

#include "../GameLayout.h"
#include "../../../../../global/gameInfo/GameInfo.h"

class CheckersLayout : public GameLayout{
public:
    explicit CheckersLayout(QWidget* parent);
    CheckersLayout(QWidget* parent, const GameInfo& info);
};

#endif //DAMELLOPION_4_FRONT_CHECKERSLAYOUT_H
