#include "CheckersLayout.h"
#include "../../../views/checkers/CheckersView.h"

CheckersLayout::CheckersLayout(QWidget *parent) : GameLayout(10, 10, parent)
{
    auto* checkersView = new CheckersView(this, 10, 10);
    checkersView->updateView();
    _mainLayout->addWidget(checkersView, Qt::AlignVCenter);

    _gameName->setText("Checkers");
    _status->setText("Turn of player 1");
}

CheckersLayout::CheckersLayout(QWidget *parent, const GameInfo& info) : GameLayout(10, 10, parent)
{
    auto* checkersView = new CheckersView(this, 10, 10, info);
    _mainLayout->addWidget(checkersView, Qt::AlignVCenter);

    _gameName->setText("Checkers");
    std::string status = "Turn of player " + std::to_string(info.getCurrentPlayer() + 1);
    _status->setText(QString::fromStdString(status));
    checkersView->updateView();
}