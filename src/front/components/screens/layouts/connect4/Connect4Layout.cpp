#include "Connect4Layout.h"

Connect4Layout::Connect4Layout(QWidget* parent) : GameLayout(6,7, parent)
{
    _connect4View = new Connect4View(this, 6, 7);
    _mainLayout->addWidget(_connect4View, Qt::AlignVCenter);

    _gameName->setText("Connect 4");
    _status->setText("Turn of player 1");
}

Connect4Layout::Connect4Layout(QWidget* parent, const GameInfo& info) : GameLayout(6,7, parent)
{
    _connect4View = new Connect4View(this, 6, 7, info);
    _mainLayout->addWidget(_connect4View, Qt::AlignVCenter);

    _gameName->setText("Connect 4");

    std::string status = "Turn of player " + std::to_string(info.getCurrentPlayer() + 1);
    _status->setText(QString::fromStdString(status));

    _connect4View->updateView();
}