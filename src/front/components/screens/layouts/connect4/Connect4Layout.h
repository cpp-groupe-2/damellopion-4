#ifndef DAMELLOPION_4_FRONT_CONNECT4LAYOUT_H
#define DAMELLOPION_4_FRONT_CONNECT4LAYOUT_H

#include "../GameLayout.h"
#include "../../../../../global/gameInfo/GameInfo.h"
#include "../../../views/connect4/Connect4View.h"

class Connect4Layout : public GameLayout
{
public:
    explicit Connect4Layout(QWidget* parent);
    explicit Connect4Layout(QWidget* parent, const GameInfo& info);
private:
    Connect4View* _connect4View;
};


#endif //DAMELLOPION_4_FRONT_CONNECT4LAYOUT_H
