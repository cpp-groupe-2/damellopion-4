#ifndef DAMELLOPION_4_FRONT_GAMELAYOUT_H
#define DAMELLOPION_4_FRONT_GAMELAYOUT_H

#include <QWidget>
#include <QLabel>
#include <QBoxLayout>

class GameLayout : public QWidget
{

public:
    GameLayout(int rowSize, int columnSize, QWidget *parent);
    void changeStatus(const std::string& statusMessage);
protected:
    QBoxLayout* _mainLayout;
    QLabel* _gameName;
    QLabel* _status;
};


#endif //DAMELLOPION_4_FRONT_GAMELAYOUT_H
