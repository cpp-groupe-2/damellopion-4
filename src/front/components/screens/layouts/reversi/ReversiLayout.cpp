#include "ReversiLayout.h"

ReversiLayout::ReversiLayout(QWidget *parent) : GameLayout(8, 8, parent)
{
    _reversiView = new ReversiView(this, 8, 8);
    _reversiView->updateView();
    _mainLayout->addWidget(_reversiView, Qt::AlignVCenter);

    _gameName->setText("Reversi");
    _status->setText("Turn of player 1");
}

ReversiLayout::ReversiLayout(QWidget *parent, const GameInfo& info) : GameLayout(8, 8, parent)
{
    _reversiView = new ReversiView(this, 8, 8, info);
    _mainLayout->addWidget(_reversiView, Qt::AlignVCenter);

    _gameName->setText("Reversi");

    std::string status = "Turn of player " + std::to_string(info.getCurrentPlayer() + 1);
    _status->setText(QString::fromStdString(status));

    _reversiView->updateView();
}
