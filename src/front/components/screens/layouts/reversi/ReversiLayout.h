#ifndef DAMELLOPION_4_FRONT_REVERSILAYOUT_H
#define DAMELLOPION_4_FRONT_REVERSILAYOUT_H

#include "../GameLayout.h"
#include "../../../../../global/gameInfo/GameInfo.h"
#include "../../../views/reversi/ReversiView.h"

class ReversiLayout : public GameLayout
{
public:
    explicit ReversiLayout(QWidget *parent);
    explicit ReversiLayout(QWidget *parent, const GameInfo& info);
private:
    ReversiView* _reversiView;
};

#endif //DAMELLOPION_4_FRONT_REVERSILAYOUT_H
