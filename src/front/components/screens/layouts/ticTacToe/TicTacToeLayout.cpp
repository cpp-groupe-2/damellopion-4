#include "TicTacToeLayout.h"

TicTacToeLayout::TicTacToeLayout(QWidget* parent) : GameLayout(3, 3, parent)
{
    _ticTacToeView = new TicTacToeView(this, 3, 3);

    _mainLayout->addWidget(_ticTacToeView, Qt::AlignVCenter);

    _gameName->setText("Tic Tac Toe");
    _status->setText("Turn of player 1");
}

TicTacToeLayout::TicTacToeLayout(QWidget *parent, const GameInfo& info) : GameLayout(3, 3, parent)
{
    _ticTacToeView = new TicTacToeView(this, 3, 3, info);

    _mainLayout->addWidget(_ticTacToeView, Qt::AlignVCenter);

    _gameName->setText("Tic Tac Toe");

    std::string status = "Turn of player " + std::to_string(info.getCurrentPlayer() + 1);
    _status->setText(QString::fromStdString(status));

    _ticTacToeView->updateView();
}
