#ifndef DAMELLOPION_4_FRONT_TICTACTOELAYOUT_H
#define DAMELLOPION_4_FRONT_TICTACTOELAYOUT_H

#include "../GameLayout.h"
#include "../../../../../back/games/ticTacToe/TicTacToeGame.h"
#include "../../../../../global/gameInfo/GameInfo.h"
#include "../../../views/ticTacToe/TicTacToeView.h"

class TicTacToeLayout : public GameLayout
{
public:
    explicit TicTacToeLayout(QWidget* parent);
    TicTacToeLayout(QWidget* parent, const GameInfo& info);
private:
    TicTacToeView* _ticTacToeView;
};


#endif //DAMELLOPION_4_FRONT_TICTACTOELAYOUT_H
