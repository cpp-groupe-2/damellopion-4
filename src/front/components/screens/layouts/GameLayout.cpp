#include <QGraphicsScene>
#include <QLabel>

#include "GameLayout.h"
#include "../../boards/Board.h"
#include "../../views/BoardView.h"

GameLayout::GameLayout(int rowSize, int columnSize, QWidget *parent)
{
    this->setParent(parent);

    setMinimumSize(columnSize * 70, rowSize * 70 + 60);

    _mainLayout = new QVBoxLayout(this);
    _mainLayout->setContentsMargins(0, 0, 0, 0 );
    _mainLayout->setSpacing(0);
    _mainLayout->setAlignment(Qt::AlignHCenter | Qt::AlignTop);

    _gameName = new QLabel(this);
    _gameName->setText("");
    _gameName->setFixedHeight(30);

    _status = new QLabel(this);
    _status->setText("");
    _status->setFixedHeight(30);

    _mainLayout->addWidget(_gameName);
    _mainLayout->addWidget(_status);
}

void GameLayout::changeStatus(const std::string& statusMessage)
{
    _status->setText(QString::fromStdString(statusMessage));
}
