#include "ReversiBoard.h"

ReversiBoard::ReversiBoard() : Board(8, 8)
{
    for (int row = 0; row < 8; row++)
    {
        QList<Cell*> cells;
        for (int column = 0; column < 8; column++)
        {
            Cell* cell = new Cell(this, row, column, true, QColor(157, 128, 101));
            cells.append(cell);
        }
        _cells.append(cells);
    }
}
