#include "CheckersBoard.h"

CheckersBoard::CheckersBoard() : Board(10, 10){
    for (int row = 0; row < 10; row++)
    {
        QList<Cell*> cells;
        for (int column = 0; column < 10; column++)
        {
            Cell* cell;
            if ((column + row) % 2)
            {
                cell = new Cell(this, row, column, true, QColor(157, 128, 101));
            }
            else
            {
                cell = new Cell(this, row, column, true, QColor(245, 218, 194));
            }
            cells.append(cell);
        }
        _cells.append(cells);
    }
}