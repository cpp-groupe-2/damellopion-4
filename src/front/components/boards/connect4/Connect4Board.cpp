#include "Connect4Board.h"

Connect4Board::Connect4Board() : Board(6, 7)
{
    for (int row = 0; row < 6; row++)
    {
        QList<Cell*> cells;
        for (int column = 0; column < 7; column++)
        {
            Cell* cell = new Cell(this, row, column, true, QColor(157, 128, 101));
            cells.append(cell);
        }
        _cells.append(cells);
    }
}
