#include "TicTacToeBoard.h"
#include "../../views/BoardView.h"

TicTacToeBoard::TicTacToeBoard() : Board(3, 3)
{
    for (int row = 0; row < 3; row++)
    {
        QList<Cell*> cells;
        for (int column = 0; column < 3; column++)
        {
            Cell* cell = new Cell(this, row, column, true, QColor(157, 128, 101));
            cells.append(cell);
        }
        _cells.append(cells);
    }
}
