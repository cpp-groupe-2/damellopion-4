#ifndef DAMELLOPION_4_FRONT_BOARD_H
#define DAMELLOPION_4_FRONT_BOARD_H

#include "../cells/Cell.h"
#include "../images/Image.h"

class Board : public QGraphicsRectItem
{
public:
    Board(int rowSize, int columnSize);
    QList<QList<Cell*>> getCells();

    void setImage(int row, int column, const std::string& imagePath);
    void removeImage(int row, int column);

protected:
    QList<QList<Cell*>> _cells;
    QList<Image*> _images;
    int _rowSize;
    int _columnSize;
};


#endif //DAMELLOPION_4_FRONT_BOARD_H
