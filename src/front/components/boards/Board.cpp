#include <QGraphicsScene>
#include <iostream>

#include "Board.h"

Board::Board(int rowSize, int columnSize) : _rowSize(rowSize), _columnSize(columnSize), QGraphicsRectItem() {}

QList<QList<Cell*>> Board::getCells()
{
    return _cells;
}

void Board::setImage(int row, int column, const std::string& imagePath)
{
    removeImage(row, column);
    _images.append(new Image(row, column, imagePath, this));
}

void Board::removeImage(int row, int column)
{
    long long imageIndex = -1;

    for (auto* image : _images)
    {
        if (image->getRow() != row || image->getColumn() != column)
        {
            continue;
        }
        imageIndex = _images.indexOf(image);
    }
    if (imageIndex != -1)
    {
        Image* image = _images[imageIndex];
        _images.removeAt(imageIndex);
        delete image;
    }
}