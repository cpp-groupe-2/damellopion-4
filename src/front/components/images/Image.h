#ifndef DAMELLOPION_4_FRONT_IMAGE_H
#define DAMELLOPION_4_FRONT_IMAGE_H

#include <QGraphicsRectItem>
#include <QLabel>
#include <QSvgWidget>

class Image : public QGraphicsRectItem
{
public:
    Image(int row, int column, const std::string& imagePath, QGraphicsItem *parent = nullptr);
    void setPosition(int row, int column);
    void setImage(const std::string& imagePath);
    inline int getRow() {return _row;}
    inline int getColumn() {return _column;}

private:
    QSvgWidget* _w;
    int _row;
    int _column;
};


#endif //DAMELLOPION_4_FRONT_IMAGE_H
