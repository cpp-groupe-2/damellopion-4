#include <QFile>
#include <QGraphicsProxyWidget>

#include "Image.h"

Image::Image(int row, int column, const std::string& imagePath, QGraphicsItem* parent) : QGraphicsRectItem(parent)
{
    auto* proxy = new QGraphicsProxyWidget(this);

    _w = new QSvgWidget();
    _w->setFixedSize(70, 70);
    _w->setAutoFillBackground(false);
    _w->setAttribute(Qt::WA_TranslucentBackground);

    proxy->setWidget(_w);

    setParentItem(parent);


    setPosition(row, column);
    setImage(imagePath);
}

void Image::setPosition(int row, int column)
{
    _row = row;
    _column = column;
    setPos(column * 70, row * 70);
}

void Image::setImage(const std::string& imagePath)
{
    _w->load(QString::fromStdString(imagePath));
}
