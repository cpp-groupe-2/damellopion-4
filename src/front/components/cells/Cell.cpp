#include "Cell.h"
#include "../../utils/ColorUtils.h"

Cell::Cell(QGraphicsItem* parent, int row, int column, bool isClickable, QColor color):
        _row(row), _column(column), _isClickable(isClickable), QGraphicsRectItem(parent)
{
    ColorUtils::setBgColor(this, color);
    setRect(0, 0, 70, 70);
    setPos(_column * 70, _row * 70);
}

int Cell::getRow() const
{
    return _row;
}

int Cell::getColumn() const
{
    return _column;
}

bool Cell::isClickable() const
{
    return _isClickable;
}

void Cell::setColor(QColor color)
{
    ColorUtils::setBgColor(this, color);
}

QColor Cell::getColor()
{
    return brush().color();
}