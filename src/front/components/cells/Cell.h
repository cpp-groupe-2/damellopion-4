#ifndef DAMELLOPION_4_FRONT_CELL_H
#define DAMELLOPION_4_FRONT_CELL_H

#include <QWidget>
#include <QGraphicsRectItem>
#include "../images/Image.h"

class Cell : public QGraphicsRectItem
{
public:
    Cell(QGraphicsItem* parent, int row, int column, bool isClickable, QColor color);

    int getColumn() const;
    int getRow() const;
    bool isClickable() const;
    void setColor(QColor color);
    QColor getColor();

private:
    int _row;
    int _column;
    bool _isClickable;
};


#endif //DAMELLOPION_4_FRONT_CELL_H
