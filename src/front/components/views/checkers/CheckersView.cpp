#include "CheckersView.h"

CheckersView::CheckersView(GameLayout *parent, int rowSize, int columnSize)
        : _rowSize(rowSize), _columnSize(columnSize), _game(CheckersGame()), BoardView(parent, rowSize ,columnSize)
{
    _board = new CheckersBoard();
    _scene->addItem(_board);
    _parentLayout = parent;
    resetTurn();
}

CheckersView::CheckersView(GameLayout *parent, int rowSize, int columnSize, const GameInfo& info)
        : _rowSize(rowSize), _columnSize(columnSize), _game(CheckersGame()), BoardView(parent, rowSize ,columnSize)
{
    _board = new CheckersBoard();
    _scene->addItem(_board);
    _parentLayout = parent;
    _game.load(info.getCurrentPlayer(), info.getGrid());
    resetTurn();
}

void CheckersView::resetTurn()
{
    _hasSelectedPiece = false;
    _hasSelectedDestination = false;
}

void CheckersView::mousePressEvent(QMouseEvent *event)
{
    qDebug() << "enter";
    bool needUpdate = false;

    if (event->button() != Qt::LeftButton)
    {
        return;
    }

    const std::pair<int, int> cellPos = getClickedCell(event->pos());

    if (cellPos.first == -1 || cellPos.second == -1)
    {
        return;
    }

    if (!_hasSelectedPiece){
        _pieceToMove = cellPos;
        _hasSelectedPiece = true;
        selectCell(_pieceToMove, QColor(39, 203, 192));
    }
    else if (!_hasSelectedDestination) {
        selectCell(_pieceToMove, _oldCellColor);
        _destination = cellPos;
        _hasSelectedDestination = true;
        needUpdate = _game.playTurn(_pieceToMove, _destination);
        resetTurn();
    }

    if (needUpdate)
    {
        _game.finishTurn();
        resetTurn();
        updateView();
    }
}

std::pair<int, int> CheckersView::getClickedCell(const QPointF& clickPos) const
{
    for (const QList<Cell*>& cells : _board->getCells())
    {
        for (Cell* cell : cells)
        {
            if (cell == nullptr)
            {
                continue;
            }
            const QPointF cellPos = cell->pos();

            bool isSelectedCell = clickPos.x() < cellPos.x() + cell->rect().width()
                                  && clickPos.x() > cellPos.x()
                                  && clickPos.y() < cellPos.y() + cell->rect().height()
                                  && clickPos.y() > cellPos.y();

            if (cell->isClickable() && isSelectedCell)
            {
                return {cell->getRow(), cell->getColumn()};
            }
        }
    }
    return {-1, -1};
}

void CheckersView::selectCell(std::pair<int, int> pos, QColor color)
{
    for (const QList<Cell*>& cells : _board->getCells())
    {
        for (Cell *cell: cells)
        {
            if (cell == nullptr)
            {
                continue;
            }

            if (cell->getRow() == pos.first && cell->getColumn() == pos.second)
            {
                _oldCellColor = cell->getColor();
                cell->setColor(color);
            }
        }
    }
}

void CheckersView::updateView()
{
    std::vector<std::vector<Token>> grid = _game.getGrid().getGrid();

    for (int row = 0; row < _rowSize; row++)
    {
        for (int column = 0; column < _columnSize; column++)
        {
            const Token& token = grid[row][column];
            if (token.getPlayer() == -1)
            {
                _board->removeImage(row, column);
                continue;
            }
            _board->setImage(row, column, token.getImagePath());
        }
    }

    if(_game.isFinished())
    {
        _parentLayout->changeStatus("Finished - Winner is player " + std::to_string(_game.getCurrentPlayer()+1));
    }
    else
    {
        _parentLayout->changeStatus("Turn of player " + std::to_string(_game.getCurrentPlayer()+1));
    }
}
