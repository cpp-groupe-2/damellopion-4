#ifndef DAMELLOPION_4_FRONT_CHECKERSVIEW_H
#define DAMELLOPION_4_FRONT_CHECKERSVIEW_H

#include "../BoardView.h"
#include "../../screens/layouts/GameLayout.h"
#include "../../../../back/games/checkers/CheckersGame.h"
#include "../../boards/checkers/CheckersBoard.h"
#include "../../../../global/gameInfo/GameInfo.h"

class CheckersView : public BoardView {
public:
    CheckersView(GameLayout* parent, int rowSize, int columnSize);
    CheckersView(GameLayout* parent, int rowSize, int columnSize, const GameInfo& info);
    void mousePressEvent(QMouseEvent *event) override;
    std::pair<int, int> getClickedCell(const QPointF &clickPos) const;
    void updateView() override;

private:
    int _rowSize;
    int _columnSize;

    GameLayout* _parentLayout;
    CheckersGame _game;
    CheckersBoard* _board;
    std::pair<int, int> _pieceToMove;
    std::pair<int, int> _destination;
    bool _hasSelectedPiece;
    bool _hasSelectedDestination;

    QColor _oldCellColor;

    void resetTurn();

    void selectCell(std::pair<int, int> pair, QColor color);
};
#endif //DAMELLOPION_4_FRONT_CHECKERSVIEW_H
