#ifndef DAMELLOPION_4_FRONT_TICTACTOEVIEW_H
#define DAMELLOPION_4_FRONT_TICTACTOEVIEW_H

#include "../BoardView.h"
#include "../../boards/ticTacToe/TicTacToeBoard.h"
#include "../../../../back/games/ticTacToe/TicTacToeGame.h"
#include "../../screens/layouts/GameLayout.h"
#include "../../../../global/gameInfo/GameInfo.h"

class TicTacToeView : public BoardView
{
public:
    TicTacToeView(GameLayout* parent, int rowSize, int columnSize);
    TicTacToeView(GameLayout* parent, int rowSize, int columnSize, const GameInfo& info);
    void mousePressEvent(QMouseEvent *event) override;
    std::pair<int, int> getClickedCell(const QPointF& clickPos) const;

    void updateView() override;

private:
    int _rowSize;
    int _columnSize;

    GameLayout* _parentLayout;
    TicTacToeGame _game;
    TicTacToeBoard* _board;
};


#endif //DAMELLOPION_4_FRONT_TICTACTOEVIEW_H
