#ifndef DAMELLOPION_4_FRONT_REVERSIVIEW_H
#define DAMELLOPION_4_FRONT_REVERSIVIEW_H

#include "../BoardView.h"
#include "../../screens/layouts/GameLayout.h"
#include "../../../../back/games/reversi/ReversiGame.h"
#include "../../boards/reversi/ReversiBoard.h"
#include "../../../../global/gameInfo/GameInfo.h"

class ReversiView : public BoardView
{
public:
    ReversiView(GameLayout* parent, int rowSize, int columnSize);
    ReversiView(GameLayout* parent, int rowSize, int columnSize, const GameInfo& info);
    void mousePressEvent(QMouseEvent *event) override;
    std::pair<int, int> getClickedCell(const QPointF& clickPos) const;

    void updateView() override;

private:
    int _rowSize;
    int _columnSize;

    GameLayout* _parentLayout;
    ReversiGame _game;
    ReversiBoard* _board;
};


#endif //DAMELLOPION_4_FRONT_REVERSIVIEW_H
