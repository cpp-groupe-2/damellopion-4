#include "ReversiView.h"

ReversiView::ReversiView(GameLayout *parent, int rowSize, int columnSize)
    : _rowSize(rowSize), _columnSize(columnSize), _game(ReversiGame()), BoardView(parent, rowSize ,columnSize)
{
    _board = new ReversiBoard();

    _scene->addItem(_board);
    _parentLayout = parent;
}

ReversiView::ReversiView(GameLayout *parent, int rowSize, int columnSize, const GameInfo& info)
        : _rowSize(rowSize), _columnSize(columnSize), _game(ReversiGame()), BoardView(parent, rowSize ,columnSize)
{
    _board = new ReversiBoard();

    _scene->addItem(_board);
    _parentLayout = parent;

    _game.load(info.getCurrentPlayer(), info.getGrid());
}

void ReversiView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton)
    {
        return;
    }

    const std::pair<int, int> cellPos = getClickedCell(event->pos());

    if (cellPos.first == -1 || cellPos.second == -1)
    {
        return;
    }

    bool needUpdate = _game.playTurn(cellPos);
    if (needUpdate)
    {
        updateView();
    }
}

std::pair<int, int> ReversiView::getClickedCell(const QPointF &clickPos) const
{
    for (const QList<Cell*>& cells : _board->getCells())
    {
        for (Cell* cell : cells)
        {
            if (cell == nullptr)
            {
                continue;
            }
            const QPointF cellPos = cell->pos();

            bool isSelectedCell = clickPos.x() < cellPos.x() + cell->rect().width()
                                  && clickPos.x() > cellPos.x()
                                  && clickPos.y() < cellPos.y() + cell->rect().height()
                                  && clickPos.y() > cellPos.y();

            if (cell->isClickable() && isSelectedCell)
            {
                return {cell->getRow(), cell->getColumn()};
            }
        }
    }

    return {-1, -1};
}

void ReversiView::updateView()
{
    std::vector<std::vector<Token>> grid = _game.getGrid().getGrid();

    for (int row = 0; row < _rowSize; row++)
    {
        for (int column = 0; column < _columnSize; column++)
        {
            const Token& token = grid[row][column];

            if (token.getPlayer() == -1 || token.getTokenType() == -1)
            {
                continue;
            }
            _board->setImage(row, column, token.getImagePath());
        }
    }

    if(_game.isFinished())
    {
        int player1Score = 0, player2Score = 0, neutralScore = 0;
        _game.getScores(player1Score, player2Score, neutralScore);

        if (player1Score > player2Score)
        {
            _parentLayout->changeStatus("Finished - Winner is player 1 with "
                + std::to_string(player1Score + neutralScore)
                + " points (Player 2's score: "
                + std::to_string(player2Score) + ")");
        }
        else if (player1Score < player2Score)
        {
            _parentLayout->changeStatus("Finished - Winner is player 2 with "
                + std::to_string(player2Score + neutralScore)
                + " points (Player 1's score: "
                + std::to_string(player1Score) + ")");
        }
        else
        {
            _parentLayout->changeStatus("Draw - " + std::to_string(player2Score) + " points");
        }
    }
    else
    {
        _parentLayout->changeStatus("Turn of player " + std::to_string(_game.getCurrentPlayer()+1));
    }
}
