#ifndef DAMELLOPION_4_FRONT_CONNECT4VIEW_H
#define DAMELLOPION_4_FRONT_CONNECT4VIEW_H

#include "../BoardView.h"
#include "../../screens/layouts/GameLayout.h"
#include "../../../../back/games/connect4/Connect4Game.h"
#include "../../boards/connect4/Connect4Board.h"
#include "../../../../global/gameInfo/GameInfo.h"

class Connect4View : public BoardView
{
public:
    Connect4View(GameLayout* parent, int rowSize, int columnSize);
    Connect4View(GameLayout* parent, int rowSize, int columnSize, const GameInfo& info);
    void mousePressEvent(QMouseEvent *event) override;
    int getClickedColumn(const QPointF& clickPos) const;

    void updateView() override;

private:
    int _rowSize;
    int _columnSize;

    GameLayout* _parentLayout;
    Connect4Game _game;
    Connect4Board* _board;
};


#endif //DAMELLOPION_4_FRONT_CONNECT4VIEW_H
