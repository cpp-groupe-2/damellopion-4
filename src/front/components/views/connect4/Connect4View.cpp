#include "Connect4View.h"
#include "../../../../global/gameInfo/GameInfo.h"

Connect4View::Connect4View(GameLayout *parent, int rowSize, int columnSize)
    : _rowSize(rowSize), _columnSize(columnSize), _game(Connect4Game()), BoardView(parent, rowSize ,columnSize)
{
    _board = new Connect4Board();
    _scene->addItem(_board);
    _parentLayout = parent;
}

Connect4View::Connect4View(GameLayout *parent, int rowSize, int columnSize, const GameInfo& info)
        : _rowSize(rowSize), _columnSize(columnSize), _game(Connect4Game()), BoardView(parent, rowSize ,columnSize)
{
    _board = new Connect4Board();
    _scene->addItem(_board);
    _parentLayout = parent;
    _game.load(info.getCurrentPlayer(), info.getGrid());
}

void Connect4View::mousePressEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton)
    {
        return;
    }

    const int column = getClickedColumn(event->pos());

    if (column == -1)
    {
        return;
    }

    bool needUpdate = _game.playTurn(column);
    if (needUpdate)
    {
        updateView();
    }
}

int Connect4View::getClickedColumn(const QPointF &clickPos) const
{
    for (const QList<Cell*>& cells : _board->getCells())
    {
        for (Cell* cell : cells)
        {
            if (cell == nullptr)
            {
                continue;
            }
            const QPointF cellPos = cell->pos();

            bool isSelectedCell = clickPos.x() < cellPos.x() + cell->rect().width()
                                  && clickPos.x() > cellPos.x()
                                  && clickPos.y() < cellPos.y() + cell->rect().height()
                                  && clickPos.y() > cellPos.y();

            if (cell->isClickable() && isSelectedCell)
            {
                return cell->getColumn();
            }
        }
    }

    return -1;
}

void Connect4View::updateView()
{
    std::vector<std::vector<Token>> grid = _game.getGrid().getGrid();

    for (int row = 0; row < _rowSize; row++)
    {
        for (int column = 0; column < _columnSize; column++)
        {
            const Token& token = grid[row][column];
            if (token.getPlayer() == -1)
            {
                continue;
            }
            _board->setImage(row, column, token.getImagePath());
        }
    }

    if(_game.isFinished())
    {
        if (_game.isDraw())
        {
            _parentLayout->changeStatus("Draw");
        }
        else
        {
            _parentLayout->changeStatus("Finished - Winner is player " + std::to_string(_game.getCurrentPlayer()+1));
        }
    }
    else
    {
        _parentLayout->changeStatus("Turn of player " + std::to_string(_game.getCurrentPlayer()+1));
    }
}
