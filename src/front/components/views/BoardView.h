#ifndef DAMELLOPION_4_FRONT_BOARDVIEW_H
#define DAMELLOPION_4_FRONT_BOARDVIEW_H

#include <QMouseEvent>
#include <QGraphicsView>
#include <QGraphicsScene>

class BoardView : public QGraphicsView
{
public:
    BoardView(QWidget* parent, int rowSize, int columnSize);
    void mousePressEvent(QMouseEvent *event) override = 0;

    virtual void updateView() = 0;

protected:
    QGraphicsScene* _scene;
};


#endif //DAMELLOPION_4_FRONT_BOARDVIEW_H
