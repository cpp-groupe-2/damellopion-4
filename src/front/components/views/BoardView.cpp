#include <iostream>
#include "BoardView.h"
#include "../boards/Board.h"

BoardView::BoardView(QWidget* parent, int rowSize, int columnSize) : QGraphicsView(parent)
{
    setParent(parent);
    setFixedSize(70 * columnSize, 70 * rowSize);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFrameStyle(QFrame::NoFrame);

    _scene = new QGraphicsScene();
    _scene->setSceneRect(0, 0, columnSize * 70, rowSize * 70);
    setScene(_scene);

    QBrush brush;
    brush.setStyle((Qt::SolidPattern));
    brush.setColor(QColor(255, 0, 0));
    setBackgroundBrush(brush);
}