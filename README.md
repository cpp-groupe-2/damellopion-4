# Damellopion 4

Projet C++ permettant de jouer aux jeux de sociétés suivants :
- Morpion
- Puissance 4
- Othello
- Dames

## Membres du groupe
- Leo SEGUIN
- Benjamin HAMM
- Julien DUBOCAGE
- Benoît ALLARD

## Requirements pour build

- C++ 17
- QT Framework version 6


FIP 1A - Module C++